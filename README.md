# nasa_applet_port

# How to run the programs
Video demonstration of how to run the programs available here: https://youtu.be/-ky4Pi1GJnM.

## Download Standalone Java Application
* https://github.com/hughpearse/NASAUCPJavaStandalone/archive/master.zip.
* Extract archive to desired directory

## Setup Java Runtime Environment
* https://www.java.com/en/download/win10.jsp (assuming windows 10).
* Open command prompt.
* Test java installed by executing command: ```java -version``` which should return something like the image below.

![java -version](img/version_check.jpg)


## Run Application
* Open command prompt if not already open.
* Navigate to directory of extracted archive.
* Execute command ```java Driver```.

--------
# Notes
Originally this repository was to port the applets available at https://www.grc.nasa.gov/www/k-12/UndergradProgs/index.htm.

However, after starting the porting, I discovered many applets had already been ported at: https://github.com/hughpearse/NASAUCPJavaStandalone.
